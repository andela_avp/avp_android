package com.andela.andelavotingplatform.model

/**
 * Created by Eston on 10/12/2017.
 */
data class VotingCheckResult(
        var award: Award = Award(),
        var alreadyVoted: Boolean = false,
        var nomineeVotedFor: Nominee = Nominee()
)