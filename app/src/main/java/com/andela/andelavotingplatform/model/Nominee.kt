package com.andela.andelavotingplatform.model

/**
 * Created by Eston on 06/12/2017.
 */
data class Nominee(
        var id: String = "",
        var email: String = "",
        var name: String = ""
)