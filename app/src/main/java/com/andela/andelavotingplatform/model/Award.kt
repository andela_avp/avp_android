package com.andela.andelavotingplatform.model

/**
 * Created by Eston on 06/12/2017.
 */
data class Award(
        var id: String = "",
        var nominees: List<Nominee> = listOf(),
        var startTime: String = "",
        var endTime: String = "",
        var title: String = "",
        var votingOpen: Boolean = false
)