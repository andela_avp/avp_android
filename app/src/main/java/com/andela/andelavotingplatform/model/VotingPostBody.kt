package com.andela.andelavotingplatform.model

/**
 * Created by Eston on 10/12/2017.
 */
data class VotingPostBody (
        var nomineeId: String,
        var awardId: String,
        var userId: String
)