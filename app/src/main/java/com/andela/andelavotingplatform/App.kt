package com.andela.andelavotingplatform

import android.app.Application
import timber.log.Timber
import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric



/**
 * Created by Eston on 05/12/2017.
 */
class App : Application() {
    override fun onCreate() {
        super.onCreate()
        setUpTimber()
        initCrashlytics()
    }

    private fun initCrashlytics() {
        if (!BuildConfig.DEBUG) {
            Fabric.with(this, Crashlytics())
        }
    }

    private fun setUpTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}