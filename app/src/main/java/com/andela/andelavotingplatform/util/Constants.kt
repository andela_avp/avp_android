package com.emtech.zuru.util

/**
 * Created by Karumbi on 27/07/2017.
 */

const val GOOGLE_REQUEST = 3456
const val  AWARDS_REF = "awards"
const val VOTING_OPEN = "votingOpen"
const val VOTES = "votes"
const val TITLE = "title"
const val NOMINEES = "nominees"
const val START_TIME = "startTime"
const val END_TIME = "endTime"
const val USERS = "users"
const val DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ"