package com.andela.andelavotingplatform.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import com.andela.andelavotingplatform.model.GenericResult
import com.andela.andelavotingplatform.model.VotingCheckResult
import com.andela.andelavotingplatform.model.VotingPostBody
import com.andela.andelavotingplatform.network.getCloudApi
import com.emtech.zuru.util.AWARDS_REF
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber


/**
 * Created by Eston on 06/12/2017.
 */
class VotingViewModel(app: Application) : AndroidViewModel(app) {

    val awardObservable = SingleLiveEvent<Pair<Boolean, VotingCheckResult?>>()
    val votedObservable = SingleLiveEvent<Boolean>()
    var selectedPosition: Int = -1

    fun fetchOpenAward() {
        setUpFirebaseRealtime()
    }

    private fun cloudFunctionFetch() {
        val firebaseAuth = FirebaseAuth.getInstance()
        val userId = firebaseAuth.currentUser?.uid!!
        val apiService = getCloudApi()
        apiService.checkVotingStatus(userId)
                .enqueue(object : Callback<VotingCheckResult> {
                    override fun onResponse(call: Call<VotingCheckResult>?, response: Response<VotingCheckResult>?) {
                        if (response?.code() == 200) {
                            awardObservable.value = Pair(true, response.body())
                        } else {
                            awardObservable.value = Pair(false, null)
                        }
                    }

                    override fun onFailure(call: Call<VotingCheckResult>?, t: Throwable?) {
                        awardObservable.value = Pair(false, null)
                    }
                })
    }

    private fun setUpFirebaseRealtime() {
        val database = FirebaseDatabase.getInstance()
        database.getReference(AWARDS_REF)
                .addChildEventListener(object : ChildEventListener {
                    override fun onCancelled(p0: DatabaseError?) {

                    }

                    override fun onChildMoved(p0: DataSnapshot?, p1: String?) {

                    }

                    override fun onChildChanged(p0: DataSnapshot?, p1: String?) {
                        Timber.d("onChildChanged")
                        cloudFunctionFetch()
                    }

                    override fun onChildAdded(p0: DataSnapshot?, p1: String?) {
                        Timber.d("onChildAdded")
                        cloudFunctionFetch()
                    }

                    override fun onChildRemoved(p0: DataSnapshot?) {
                        Timber.d("onChildRemoved")
                        cloudFunctionFetch()
                    }

                })
    }

    fun postVote(nomineeId: String, awardId: String) {
        Timber.d("Award ID: $awardId")
        Timber.d("Nominee ID: $nomineeId")
        val firebaseAuth = FirebaseAuth.getInstance()
        val userId = firebaseAuth.currentUser?.uid!!
        Timber.d("User ID: $userId")
        val apiService = getCloudApi()
        apiService.postVote(VotingPostBody(
                nomineeId = nomineeId,
                awardId = awardId,
                userId = userId
        ))
                .enqueue(object : Callback<GenericResult> {
                    override fun onFailure(call: Call<GenericResult>?, t: Throwable?) {
                        votedObservable.value = false
                    }

                    override fun onResponse(call: Call<GenericResult>?, response: Response<GenericResult>?) {
                        votedObservable.value = response?.code() == 201
                    }
                })
    }

}