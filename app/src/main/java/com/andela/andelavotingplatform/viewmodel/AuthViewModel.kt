package com.andela.andelavotingplatform.viewmodel

import android.app.Activity
import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.content.Intent
import android.support.v4.app.FragmentActivity
import com.andela.andelavotingplatform.BuildConfig
import com.emtech.zuru.util.GOOGLE_REQUEST
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import timber.log.Timber

/**
 * Created by Eston on 05/12/2017.
 */
class AuthViewModel(app: Application) : AndroidViewModel(app) {

    private val firebaseAuth: FirebaseAuth = FirebaseAuth.getInstance()
    private var googleApiClient: GoogleApiClient? = null
    val loginObservable: SingleLiveEvent<Pair<Boolean, String>> = SingleLiveEvent()

    fun googleLogin(activity: Activity) {
        if (googleApiClient == null) {
            val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestIdToken(BuildConfig.GOOGLE_WEB_CLIENT_ID)
                    .requestEmail()
                    .requestProfile()
                    .build()
            googleApiClient = GoogleApiClient.Builder(activity)
                    .enableAutoManage(activity as FragmentActivity) { }
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build()
        }
        val signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient)
        activity.startActivityForResult(signInIntent, GOOGLE_REQUEST)
    }

    private fun firebaseAuthWithGoogle(account: GoogleSignInAccount?) {
        val credential = GoogleAuthProvider.getCredential(account?.idToken, null)
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(firebaseCompleteListener)
    }

    fun handleActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            GOOGLE_REQUEST -> handleGoogleSignIn(data)
        }
    }

    private fun handleGoogleSignIn(data: Intent?) {
        val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
        if (result.isSuccess) {
            val account = result.signInAccount
            if (!BuildConfig.DEBUG) {
                if (result.signInAccount?.email!!.contains("andela")) {
                    firebaseAuthWithGoogle(account)
                } else {
                    Timber.d("Login error: not Andela account")
                    loginObservable.value = Pair(false, "Please use an Andela email account to sign in.")
                    Auth.GoogleSignInApi.signOut(googleApiClient)
                }
            } else {
                firebaseAuthWithGoogle(account)
            }

        } else {
            Timber.d("Firebase auth failed: %s", result.status.statusCode)
        }
    }

    private val firebaseCompleteListener = OnCompleteListener<AuthResult> {
        Timber.d("OAuth call complete")
        if (it.isSuccessful) {
            val user = firebaseAuth.currentUser
            if (user != null) {
                loginObservable.value = Pair(true, "Welcome, ${user.displayName}!")
            } else {
                val error = it.exception.toString()
                Timber.d("Auth failed: %s", error)
            }
        } else {

        }
    }

}