package com.andela.andelavotingplatform.shared.widget

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.widget.RadioButton
import android.widget.RadioGroup
import com.andela.andelavotingplatform.R
import com.andela.andelavotingplatform.model.Nominee

@SuppressLint("ViewConstructor")
/**
 * Created by Eston on 06/12/2017.
 */
class NomineeRadioButton(context: Context?, parent: RadioGroup, nominee: Nominee) : RadioButton(context) {
    init {
        LayoutInflater.from(context)
                .inflate(R.layout.radio_button_layout, parent, false)
        text = nominee.name
    }
}