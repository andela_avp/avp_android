package com.andela.andelavotingplatform.network

import com.andela.andelavotingplatform.BuildConfig
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import okhttp3.logging.HttpLoggingInterceptor
import timber.log.Timber




/**
 * Created by Eston on 10/12/2017.
 */

fun getCloudApi(): CloudFunctionsApi {
    val baseUrl = BuildConfig.BASE_FUNCTIONS_URL
    return Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(getHttpClient())
            .addConverterFactory(MoshiConverterFactory.create())
            .build()
            .create(CloudFunctionsApi::class.java)
}

fun getHttpClient(): OkHttpClient? {
    return OkHttpClient.Builder()
            .addInterceptor(getHeaderInterceptor())
            .addInterceptor(getLoggingInterceptor())
            .build()
}

fun getLoggingInterceptor(): Interceptor? {
    val interceptor = HttpLoggingInterceptor { message -> Timber.tag("OkHttp").d(message) }
    interceptor.level = HttpLoggingInterceptor.Level.BODY
    return interceptor
}

fun getHeaderInterceptor(): Interceptor? {
    return Interceptor {
        val original = it.request()
        val url = original.url()
        val newUrl = url.newBuilder()
                .build()
        val builder = original.newBuilder().url(newUrl)
        val request = builder
                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                .build()
        it.proceed(request)
    }
}
