package com.andela.andelavotingplatform.network

import com.andela.andelavotingplatform.model.GenericResult
import com.andela.andelavotingplatform.model.VotingCheckResult
import com.andela.andelavotingplatform.model.VotingPostBody

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

/**
 * Created by Eston on 10/12/2017.
 */

interface CloudFunctionsApi {
    @GET("fetchVotingData/")
    fun checkVotingStatus(@Query("userId") userId: String): Call<VotingCheckResult>

    @POST("postVote/")
    fun postVote(@Body votingPostBody: VotingPostBody): Call<GenericResult>
}
