package com.andela.andelavotingplatform.screens.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import com.andela.andelavotingplatform.R
import com.andela.andelavotingplatform.screens.signin.SignInActivity
import com.emtech.zuru.extension.startVotingActivity
import com.google.firebase.auth.FirebaseAuth

class Splash : AppCompatActivity() {

    private lateinit var firebaseAuth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        Handler().postDelayed({
            firebaseAuth = FirebaseAuth.getInstance()
            if (firebaseAuth.currentUser == null)
                showSignupOptions()
            else
                startVotingActivity()
        }, 500)
    }

    private fun showSignupOptions() {
        val intent = Intent(this, SignInActivity::class.java)
        startActivity(intent)
        finish()
    }

}