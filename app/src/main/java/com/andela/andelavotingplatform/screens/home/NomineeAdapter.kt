package com.andela.andelavotingplatform.screens.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.View.*
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.andela.andelavotingplatform.R
import com.andela.andelavotingplatform.model.Nominee
import com.andela.andelavotingplatform.viewmodel.VotingViewModel
import timber.log.Timber

/**
 * Created by Eston on 11/12/2017.
 */
class NomineeAdapter(
        context: Context?,
        private val nominees: List<Nominee>,
        private val viewModel: VotingViewModel)
    : ArrayAdapter<Nominee>(context, 0, nominees) {
    private var views: MutableList<View> = mutableListOf()
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View
        if ((position + 1) <= views.size) {
            view = views[position]
        } else {
            view = LayoutInflater.from(context)
                    .inflate(R.layout.nominee_list_item, parent, false)
            view.findViewById<TextView>(R.id.nominee_name).text = nominees[position].name
            views.add(view)
        }
        return view
    }

    fun showAsSelected(position: Int) {
        Timber.d("Selected position: $position")
        viewModel.selectedPosition = position
        views.forEachIndexed { index, view ->
            val check = view.findViewById<View>(R.id.nominee_check)
            val visibility = if (index == position) VISIBLE else INVISIBLE
            val background = if (index == position) R.color.voted_bg else R.color.white
            check.visibility = visibility
            view.setBackgroundResource(background)
        }
    }
}