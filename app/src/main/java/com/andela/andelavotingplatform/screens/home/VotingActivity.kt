package com.andela.andelavotingplatform.screens.home

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Vibrator
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.text.Html
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.TextView
import com.afollestad.materialdialogs.MaterialDialog
import com.andela.andelavotingplatform.R
import com.andela.andelavotingplatform.model.Award
import com.andela.andelavotingplatform.model.Nominee
import com.andela.andelavotingplatform.viewmodel.VotingViewModel
import com.emtech.zuru.extension.computeDiff
import com.emtech.zuru.extension.showErrorDialog
import com.emtech.zuru.extension.showProgressDialog
import kotlinx.android.synthetic.main.activity_voting.*
import timber.log.Timber
import java.util.*
import java.util.concurrent.TimeUnit


class VotingActivity : AppCompatActivity() {

    private lateinit var viewModel: VotingViewModel

    private lateinit var dialog: MaterialDialog

    private lateinit var award: Award

    private lateinit var nominees: List<Nominee>

    private lateinit var selectedNominee: String

    private val handler: Handler = Handler()

    private var firstVibration = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_voting)
        setSupportActionBar(toolbar)
        setButtonListeners()
        viewModel = ViewModelProviders.of(this).get(VotingViewModel::class.java)
        prepObservables()
        dialog = showProgressDialog("Fetching currently open award")
        viewModel.fetchOpenAward()
    }

    private fun prepObservables() {
        viewModel.awardObservable.observe(this, Observer {
            dialog.dismiss()
            if (it!!.first) {
                val result = it.second!!
                award = result.award
                if (result.alreadyVoted) {
                    displayVotedInterface(result.nomineeVotedFor)
                } else {
                    displayVoteInterface()
                }
            } else {
               displayNoAwardInterface()
            }
        })
        viewModel.votedObservable.observe(this, Observer {
            dialog.dismiss()
            if (it!!) {
                val votedNominee = nominees.filter { it.id == selectedNominee }[0]
                displayVotedInterface(votedNominee)
            } else {
                Snackbar.make(content, "Voting failed, please try again.", Snackbar.LENGTH_LONG)
                        .show()
            }
        })
    }

    private fun vibrate() {
        Timber.d("First vibration: $firstVibration")
        if (firstVibration) {
            firstVibration = false
            val v = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
            v.vibrate(500)
        }
    }

    private fun setButtonListeners() {
        vote_action.setOnClickListener {
            if (viewModel.selectedPosition >= 0) {
                dialog = showProgressDialog("Posting vote")
                selectedNominee = nominees[viewModel.selectedPosition].id
                viewModel.postVote(selectedNominee, award.id)
            } else {
                showErrorDialog("Error", "Please pick one nominee.")
            }
        }
        change_vote_button.setOnClickListener {
            displayVoteInterface()
        }

    }

    private fun setHtmlText(htmlText: String, target: TextView) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            target.text = Html.fromHtml(htmlText, Html.FROM_HTML_MODE_COMPACT)
        } else {
            target.text = Html.fromHtml(htmlText)
        }
    }

    private fun displayVotedInterface(nominee: Nominee) {
        resetAll()
        voted_layout.visibility = VISIBLE
        award_title.text = award.title
        val htmlText = getString(R.string.already_voted, nominee.name)
        setHtmlText(htmlText, already_voted_text)
        firstVibration = false
    }

    private fun displayVoteInterface() {
        resetAll()
        vibrate()
        vote_action.visibility = VISIBLE
        voting_layout.visibility = VISIBLE
        nominees = award.nominees
        award_title.text = award.title
        loadNominees()
        handler.removeCallbacks(timerHandler)
        showVotingText()
        displayVotingTimer()
    }

    private fun displayNoAwardInterface() {
        resetAll()
        no_open_awards_layout.visibility = VISIBLE
        firstVibration = true
    }

    private fun resetAll() {
        viewModel.selectedPosition = -1
        no_open_awards_layout.visibility = GONE
        voted_layout.visibility = GONE
        voting_layout.visibility = GONE
        vote_action.visibility = GONE
        voting_end.visibility = GONE
        award_title.setText(R.string.avp_title)
    }

    private fun loadNominees() {
        val adapter = NomineeAdapter(this, nominees, viewModel)
        nominees_listview.adapter = null
        nominees_listview.adapter = adapter
        nominees_listview.setOnItemClickListener { adapterView, view, i, l ->
            adapter.showAsSelected(i)
        }
        adapter.notifyDataSetChanged()
    }

    private val timerHandler: Runnable = Runnable {
        try {
            showVotingText()
            displayVotingTimer()
        } catch (e: Exception) {
            Timber.d("Timer display error: ${e.message}")
            voting_end.visibility = GONE
        }
    }

    @SuppressLint("StringFormatMatches")
    private fun showVotingText() {
        try {
            voting_end.visibility = VISIBLE
            val timestamp = award.endTime.toLong()
            val dateToEnd = Date(timestamp)
            val timeMap = computeDiff(Date(), dateToEnd)
            if (timeMap[TimeUnit.SECONDS]!! <= 0L) {
                displayNoAwardInterface()
            } else {
                val timeLeftString = getString(
                        R.string.time_left,
                        "${timeMap[TimeUnit.DAYS]}",
                        doubleDigitFormat(timeMap[TimeUnit.HOURS]),
                        doubleDigitFormat(timeMap[TimeUnit.MINUTES]),
                        doubleDigitFormat(timeMap[TimeUnit.SECONDS])
                )
                setHtmlText(timeLeftString, voting_end)
            }
        } catch (e: Exception) {
            Timber.d("Timer display error: ${e.message}")
            voting_end.visibility = GONE
        }
    }

    private fun displayVotingTimer() {
        handler.postDelayed(timerHandler, 1000)
    }

    private fun doubleDigitFormat(input: Long?): String {
        return String.format("%02d", input)
    }
}
