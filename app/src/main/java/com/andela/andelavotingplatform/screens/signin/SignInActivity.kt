package com.andela.andelavotingplatform.screens.signin

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import com.afollestad.materialdialogs.MaterialDialog
import com.andela.andelavotingplatform.R
import com.andela.andelavotingplatform.viewmodel.AuthViewModel
import com.emtech.zuru.extension.showProgressDialog
import com.emtech.zuru.extension.startVotingActivity
import kotlinx.android.synthetic.main.activity_sign_in.*
import timber.log.Timber

class SignInActivity : AppCompatActivity() {

    private lateinit var authViewModel: AuthViewModel
    private lateinit var progressDialog: MaterialDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)
        authViewModel = ViewModelProviders.of(this).get(AuthViewModel::class.java)
        authViewModel.loginObservable.observe(this, Observer {
            progressDialog.dismiss()
            it.let {
                Snackbar.make(content_sign_in, it!!.second, Snackbar.LENGTH_LONG)
                        .show()
                if (it.first) {
                    startVotingActivity()
                }
            }
        })
        ripple.setOnClickListener {
            Timber.d("Ripple!")
            progressDialog = showProgressDialog("Signing in")
            authViewModel.googleLogin(this)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        authViewModel.handleActivityResult(requestCode, resultCode, data)
    }
}