package com.emtech.zuru.extension

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.widget.EditText
import com.afollestad.materialdialogs.MaterialDialog
import com.andela.andelavotingplatform.R
import com.andela.andelavotingplatform.screens.home.VotingActivity
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit


/**
 * Created by Karumbi on 27/07/2017.
 */

fun String.getStringPref(context: Context): String? {
    val preferences = getPreferences(context)
    return preferences.getString(this, null)
}

fun getPreferences(context: Context): SharedPreferences {
    return PreferenceManager.getDefaultSharedPreferences(context)
}

fun AppCompatActivity.showErrorDialog(title: String = "", message: String) {
    MaterialDialog.Builder(this)
            .title(title)
            .content(message)
            .positiveText(getString(R.string.label_okay))
            .show()
}

fun AppCompatActivity.showProgressDialog(title: String): MaterialDialog {
    return MaterialDialog.Builder(this)
            .title(title)
            .content(getString(R.string.please_wait))
            .progress(true, 0)
            .cancelable(false)
            .show()
}

fun AppCompatActivity.startVotingActivity() {
    val intent = Intent(this, VotingActivity::class.java)
    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
    startActivity(intent)
}

fun EditText.setDate(date: Date) {
    if ("English" == Locale.getDefault().displayLanguage) {
        var tmp = SimpleDateFormat("MMMM d")
        var str = tmp.format(date)
        str = str.substring(0, 1).toUpperCase() + str.substring(1)
        if (date.date in 11..13)
            str += "th, "
        else {
            str = when {
                str.endsWith("1") -> str + "st, "
                str.endsWith("2") -> str + "nd, "
                str.endsWith("3") -> str + "rd, "
                else -> str + "th, "
            }
        }
        tmp = SimpleDateFormat("yyyy")
        str += tmp.format(date)
        setText(str)
    } else {
        val fmt = SimpleDateFormat("dd MMMM yyyy", Locale.US)
        val s = fmt.format(date)
        setText(s)
    }
}

fun AppCompatActivity.computeDiff(date1: Date, date2: Date): Map<TimeUnit, Long> {
    val diffInMillies = date2.time - date1.time
    val units = ArrayList(EnumSet.allOf(TimeUnit::class.java))
    Collections.reverse(units)
    val result = LinkedHashMap<TimeUnit, Long>()
    var milliesRest = diffInMillies
    for (unit in units) {
        val diff = unit.convert(milliesRest, TimeUnit.MILLISECONDS)
        val diffInMilliesForUnit = unit.toMillis(diff)
        milliesRest -= diffInMilliesForUnit
        result.put(unit, diff)
    }
    return result
}